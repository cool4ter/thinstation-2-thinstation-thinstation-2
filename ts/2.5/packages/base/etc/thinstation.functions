# Functions

get_debug_level()
{
    local dlevel=1 x=7 num
    num=$1
    while [ $x -gt 0 ] ; do
          let x=x-1
          let dlevel=$((num/(2**x)))
          if [ $dlevel -gt 0 ] ; then
                let num=$((num-(2**x)))
                case $x in
                0)
                  DEBUG_BOOT=1
                  ;;
                1)
                  DEBUG_NETWORK=2
                  ;;
                2)
                  DEBUG_INIT=4
                  ;;
                3)
                  DEBUG_KERNEL=8
                  ;;
                4)
                  DEBUG_MODULES=16
                  ;;
                5)
                  DEBUG_PACKAGES=32
                  ;;
                6)
                  DEBUG_EMAIL=64
                  ;;
                esac
          fi
    done
}

# If thinstation.network/user is a dos file, converts it to unix

cleanup ()
{
	if [ -e $TS_NETWORK ] ; then
		sed -e 's/\
//g' $TS_NETWORK > $TS_NETWORK.tmp
		cat $TS_NETWORK.tmp > $TS_NETWORK
		rm $TS_NETWORK.tmp
	fi
	if [ -e $TS_USER ] ; then
		sed -e 's/\
//g' $TS_USER > $TS_USER.tmp
		cat $TS_USER.tmp > $TS_USER
		rm $TS_USER.tmp
	fi

}

parse_tpl ()  
{
    if [ -f $1.tpl ]; then 
	cat $1.tpl | sed -f /tmp/script > $1
    fi
}

# Log to file and console
echo_log ()
{
	local debug noline string

	until [ -z "$1" ]; do
		case $1 in
		"-n")
			noline="-n"
		;;
		"-d")
			debug="TRUE"
		;;
		*)
			string=$1
		;;
		esac
		shift
	done
	if [ -n "$debug" ] ; then
		echo -e $noline $string > $CONSOLE
	fi
	echo -e $noline $string >> $LOGFILE
}

## PKG functions

pkg_is_loaded ()
{
    if [ -f /etc/init.d/$1 ] ; then
	return 0
    else
	return 1
    fi
}

pkg_is_executable ()
{
    if [ -x /bin/$1 ] ; then
	return 0
    else
	return 1
    fi
}

pkg_initialized ()
{
    if [ -f /var/packages/$1 ]; then
	return 0
    else
        return 1
    fi
}

pkg_set_init_flag ()
{
    touch /var/packages/$1
}

## Make variable capitals

make_caps ()
{
	if [ ! -z "$1" ]; then
		echo $1 | tr "[a-z]" "[A-Z]"
	else
		return 1
	fi
}

## Make variable lower

make_lower ()
{
	echo $1 | tr "[A-Z]" "[a-z]"
}

make_capital ()
{
   local word words badlist oneword x=0

   if [ "$1" = "-1" ] ; then
     oneword="TRUE"
     shift
   fi
   if [ $# -gt 1 ]; then
	until [ -z "$1" ]
	do
	     let x=x+1
	     case $1 in
	     VNC|TCP|ZLIB|SSL|CUPSD|JPEG|RDP|PNG|IPPPort|IPPPrinting)
		word=$1
		;;
	     *)
		if [ -z "$oneword" ] || [ $x -eq 1 ] ; then
		  word=`echo $1 | cut -c1 | tr "[a-z]" "[A-Z]"`
		  word=$word`echo $1 | cut -c2- | tr "[A-Z]" "[a-z]"`
		else
		  word=`echo $1 | tr "[A-Z]" "[a-z]"`
		fi
		;;
	     esac
	     shift
	     words="$words $word"
	done
	echo $words
   else
	case $1 in
	VNC|TCP|ZLIB|SSL|CUPSD|JPEG|RDP|PNG|IPPPort|IPPPrinting)
	   word=$1
	   ;;
	*)
	  word=`echo $1 | cut -c1 | tr "[a-z]" "[A-Z]"`
	  word=$word`echo $1 | cut -c2- | tr "[A-Z]" "[a-z]"`
	esac
	echo $word
   fi
}

# Window Manager dialogs

confirm_exit ()
{
        Xdialog --title "Logout" --clear --yesno "Click yes to Confirm Exit?" 10 60
}

dialog_get_server_address ()
{
if [ ! -z "$SERVER" ]; then
	echo $SERVER > /tmp/.dialog_$1_server
fi
if [ ! -z "$OPTIONS" ]; then
	echo "$OPTIONS" > /tmp/.dialog_$1_options
fi
if [ -e /tmp/.dialog_$1_server ] ; then
  DIALOG_SERVER=$(cat /tmp/.dialog_$1_server)
else
  touch /tmp/.dialog_$1_server
  DIALOG_SERVER="type server name here"
fi

if [ -e /tmp/.dialog_$1_options ] ; then
  DIALOG_OPTIONS=$(cat /tmp/.dialog_$1_options)
else
  touch /tmp/.dialog_$1_options
fi

# Now to business:
DIALOG_RESPONSE=`Xdialog --stdout --title $1 --left --icon /usr/icons/$1_32x32.xpm --2inputsbox "Preparing to run $1" \
                15 55 "Server name or IP number" "$DIALOG_SERVER" "Options" "$DIALOG_OPTIONS" ` 2>>$LOGFILE
if [ "$?" == "0" ] && [ ! -z "$DIALOG_RESPONSE" ] ; then
    DIALOG_LENGTH=`expr length "$DIALOG_RESPONSE"`
    DIALOG_SEPERATOR=`expr index "$DIALOG_RESPONSE" /`
    DIALOG_SERVER=`expr substr "$DIALOG_RESPONSE" 1 $(($DIALOG_SEPERATOR - 1))`
    DIALOG_OPTIONS=`expr substr "$DIALOG_RESPONSE" $(($DIALOG_SEPERATOR + 1)) $DIALOG_LENGTH`

    # preserve settings
    echo "$DIALOG_SERVER" > /tmp/.dialog_$1_server
    echo "$DIALOG_OPTIONS" > /tmp/.dialog_$1_options

    if [ "$DIALOG_SERVER" != "type server name here" ]; then
    SERVER="`echo $DIALOG_SERVER |grep -E -v '&|;|\(|\`'`"
    OPTIONS="`echo $DIALOG_OPTIONS |grep -E -v '&|;|\(|\`'`"
    fi
else
    echo "$DIALOG_SERVER" > /tmp/.dialog_$1_server
    SERVER=""
    OPTIONS=""
fi
}

dialog_no_value ()
{
        Xdialog --title "$1 Client" --clear --msgbox "You must enter value, aborted" 10 60 10000
}

dialog_get_value ()
{
        Xdialog --title "$1 Client" --clear --inputbox "Enter $2:" 10 60 2>&1 1>/dev/null |grep -E -v '&|;|\(|\`'
}

check_reconnect ()
{
  if [ -e /tmp/.X11-unix/X$DISPLAY_NUMBER ] ; then
	prompt="`make_caps $RECONNECT_PROMPT | cut -c1-4`" 
	NO_SESSION=`make_lower $NO_SESSION`
	if [ "$prompt" = "MENU" ] ; then
		prompt_time="`make_caps $RECONNECT_PROMPT | cut -c5-`" 
		if [ -n "$prompt_time" ] ; then
			kill_time=`date +%M`
			let kill_time=kill_time+prompt_time+1
			if [ $kill_time -gt 59 ] ; then
				let kill_time=kill_time-60
			fi
			echo "$kill_time  * * * * /bin/$NO_SESSION" >> /tmp/$NO_SESSION
			crontab /tmp/$NO_SESSION
		fi
		while [ "$choice_error" != "0" ] ; do
	        	choice=$(Xdialog --stdout --title "Reconnect/Shutdown?" --clear --no-cancel --combobox "Choose option then OK to continue." 10 60 Reconnect Shutdown)
			choice_error=$?
		done
		if [ "$choice" = "Shutdown" ] ; then
			$NO_SESSION
		elif [ -e /tmp/$NO_SESSION ] ; then
			crontab /tmp/crontab
			rm /tmp/$NO_SESSION
			reconnect=0
		fi
	elif [ "$prompt" = "AUTO" ] ; then
		prompt_time="`make_caps $RECONNECT_PROMPT | cut -c5-`" 
		if [ -z "$prompt_time" ] ; then
		    prompt_time=10
		fi
		Xdialog --title "No Server Response" --clear --timeout $prompt_time --msgbox "Server not responding - waiting $prompt_time seconds" 10 60
		reconnect=0
	elif [ "$prompt" == "FORCE" ]; then
		reconnect=0
	elif [ "$prompt" != "OFF" ] ; then
        	Xdialog --title "Reconnect?" --clear --yesno "Click yes to reconnect." 10 60
		reconnect=$?
	else
		reconnect=1
	fi
  fi
echo $reconnect
}

## Replace on or off with #

replace_variable ()
{
	if [ `make_caps $1` = "ON" ] ; then
		echo " "
	else
		echo "#"
	fi
}

## Replace space

replace_char ()
{
	echo $1 | sed -e "s/$2/$3/g"
}

# Check for invalid characters in options line

replace_invalid ()
{
	echo $1 | sed -e 's/ /\\ /g' | sed -e 's/\-/\\\-/g' |  sed -e 's/\//\\\//g'
}


# Overwrite file with sed command

sed_file ()
{
	cat $1 | sed -e $2 > /tmp/sed.tmp
	cat /tmp/sed.tmp > $1
	rm /tmp/sed.tmp
}

## Restart xinetd

restart_xinetd ()
{
	XPID=`pidof xinetd`
	kill -SIGHUP $XPID
}

# Checks when to reboot PC

check_reboot()
{
   if is_enabled $DAILY_REBOOT ; then
	if uptime | grep day; then
		reboot
	fi
   fi
}

# Cleans up X if it shuts down unexpected

check_xrunning()
{
	if ! ps -ef | grep "$X_SERVER" | grep -q " :$DISPLAY_NUMBER"; then
          if [ -e /tmp/.X11-unix/X$DISPLAY_NUMBER ] ; then
		rm /tmp/.X11-unix/X$DISPLAY_NUMBER
	  fi
	fi
}

# Checks to see if nxssh is running

check_NX ()
{
	while ps | grep -v grep | grep "nxssh"
	do
		sleep 5
	done
}


# Start X

start_x ()
{
	if [ -e /etc/init.d/x ] && [ ! -e /tmp/.X11-unix/X$DISPLAY_NUMBER ]; then
		/etc/init.d/x start
	elif [ ! -e /etc/init.d/x ] ; then
		echo "Can't find X" >> $LOGFILE
		sleep 1
	fi
}


# Splash Progess Bar
splash_progress ()
{
	step=`expr 65536 / $splash_total`
	pbar=`expr $progress \* $step`
		echo "progress $pbar" > ${spl_fifo}
		echo "paint" > ${spl_fifo}
}


# Check if Module loaded
check_module ()
{
	if lsmod | grep $1 > /dev/null ; then
		return 0
	else
		return 1
	fi
}

# Get Free XServer Number
getfreescreen()
{
	local X_NUMBER

        X_NUMBER=`ps -ef | grep "Xorg " | cut -f2 -d: | cut -f1 -d" "`
	x_NUMBER=$X_NUMBER `ps -ef | grep "Xnest "  | cut -f2 -d: | cut -f1 -d" "`
        let XNEST_NUMBER=0
        for x in $x_NUMBER;
        do
                if [ "$x" -ge "$XNEST_NUMBER" ] ; then
                        let XNEST_NUMBER=x+1
                fi
        done
}

get_filesystems()
{
        for filename in $FILESYSTEMS
        do
                if [ -e $MOD_PATH/inbuilt/$filename.ko ] || [ -e $MOD_PATH/loaded/$filename.ko ] ; then
			if [ "$filename" != "supermount" ] ; then
                		filesystem=$filename:$filesystem
			fi
		        if ! check_module $filename ; then
               			modprobe $filename
        		fi
                fi
        done
	let length=${#filesystem}-1
	if [ "$length" -gt 0 ] ; then
		filesystem=`echo $filesystem | cut -c 1-$length`
	fi
}

getpostcontent()
{
	CONTENT=
	if [ -n "$CONTENT_LENGTH" ]
	then
		read CONTENT
	fi
	if [ -n "$CONTENT" ]
	then
		eval `echo "&$CONTENT" | sed s/"&"/"\nCGI_"/g`
	fi
}


# Below code taken from PXES with modifications

#----------------------------------------------------------------------
#      ___       ___ ___   P X E S   Universal  Linux  Thin  Client
#     /__/\\_// /__ /__    Copyright(C) 2003 by Diego Torres Milano
#    /    // \\/__  __/    All rights reserved.  http://pxes.sf.net
#
# Author: Diego Torres Milano <diego@pxes.com.ar>
# $Id: xtools,v 1.2 2003/09/25 02:10:34 diego Exp $
#----------------------------------------------------------------------

refresh()
{
	local UPDATE=${1:-60}

	echo '
<meta HTTP-EQUIV="REFRESH" CONTENT="'$UPDATE'">
<meta HTTP-EQUIV="EXPIRES" CONTENT="'`/bin/date +%s`'">
'
}

header()
{
echo -e "Content-Type: text/html\n"
echo '
<html>

<head>
  <title>:: ThinStation ::</title>
  <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
  <link href="../style.css" rel="stylesheet" type="text/css">
</head>

<body bgcolor="#FFFFFF" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">

<tr>
	<a href="../index.html">
            <img src="../images/ts_logo.jpg" width="650" height="153" border="0" alt="Thinstation logo">
	</a>
</tr>


<h2>Thinstation '`echo $TS_VERSION`' on '`hostname`' :: '`basename $0 .cgi`'</h2>
<pre>&nbsp;</pre>
'
}

trailer()
{
	echo '</body>'
	echo '</html>'
}

# Verify's Web Password

getpasswd()
{
	local LOGIN=$1

	set +x

	if [ -z "$LOGIN" ]
	then
		echo "ERROR: Invalid login" >&2
		return
	fi

	sed -n "s/^$LOGIN:\\([^:]*\\):.*/\\1/p" < /etc/passwd
}

verify_password()
{
	local LOGIN=$1 PASSWD=$2 p s e

	if [ -z "$LOGIN" ]
	then
		echo "Null login" >&2
		return 1
	fi

	if [ -z "$PASSWD" ]
	then
		echo "Null password" >&2
		return 1
	fi

	p=`getpasswd $LOGIN`
	s=`echo "$p" | cut -f1-3 -d\$`
	e=`/bin/checkpass $PASSWD $s`

	if [ "$p" = "$e" ]
	then
		return 0
	else
		return 1
	fi
}
export spl_cachesize="4096"
export spl_cachetype="tmpfs"
export spl_cachedir="/lib/splash/cache"
export spl_tmpdir="/lib/splash/tmp"
export spl_fifo="${spl_cachedir}/.splash"
export spl_pidfile="${spl_cachedir}/daemon.pid"
export spl_util="/bin/splash_util.static"
export spl_daemon="/bin/fbsplashd.static"
export spl_decor="/bin/fbcondecor_ctl"
export spl_bindir="/bin"

splash() {
	local event="$1"
	shift

	# Reload the splash settings in rc_init.  We could have set them wrong the
	# first time splash_setup was called (when splash-functions.sh was first
	# sourced) if /proc wasn't mounted.
	if [ "${event}" = "rc_init" ]; then
		splash_setup "force"
	else
		splash_setup
	fi

	[ "${SPLASH_MODE_REQ}" = "off" ] && return

	# Prepare the cache here -- rc_init-pre might want to use it
	if [ "${event}" = "rc_init" ]; then
		if [ "${RUNLEVEL}" = "S" -a "$1" = "sysinit" ]; then
			splash_cache_prep 'start' || return
		elif [ "${RUNLEVEL}" = "6" -o "${RUNLEVEL}" = "0" ]; then
			# Check if the splash cachedir is mounted readonly. If it is,
			# we need to mount a tmpfs over it.
			if ! touch "${spl_cachedir}/message" 2>/dev/null ; then
				splash_cache_prep 'stop' || return
			fi
		fi
	fi

	local args=""

	if [ "${event}" = "rc_init" -o "${event}" = "rc_exit" ]; then
		args="$* ${RUNLEVEL}"
	elif [ "${event}" = "svc_started" -o "${event}" = "svc_stopped" ]; then
		if [ -z "$2" ]; then
			# Backwards compatibility hack.  Add a 0 to the arguments to simulate
			# an error code.
			args="$* 0"
		else
			args="$*"

			# Backwards compatibility: translate an error condition (non-zero
			# error code) into an appropriate event.
			if [ "$2" != "0" ]; then
				if [ "${event}" = "svc_started" ]; then
					event="svc_start_failed"
				else
					event="svc_stop_failed"
				fi
			fi
		fi
	else
		args="$*"
	fi

	splash_profile "pre ${event} ${args}"

	# Handle -pre event hooks
	if [ -x "/etc/splash/${SPLASH_THEME}/scripts/${event}-pre" ]; then
		/etc/splash/"${SPLASH_THEME}"/scripts/${event}-pre ${args}
	fi

	case "$event" in
		svc_start)			splash_svc_start "$1";;
		svc_stop)			splash_svc_stop "$1";;
		svc_started) 		splash_svc "$1" "start";;
		svc_stopped)		splash_svc "$1" "stop";;
		svc_start_failed)	splash_svc_fail "$1" "start";;
		svc_stop_failed)	splash_svc_fail "$1" "stop";;
		svc_input_begin)	splash_input_begin "$1";;
		svc_input_end)		splash_input_end "$1";;
		rc_init) 			splash_init "$1" "${RUNLEVEL}";;
		rc_exit) 			splash_exit "${RUNLEVEL}";;
		critical) 			splash_verbose;;
	esac

	splash_profile "post ${event} ${args}"

	# Handle -post event hooks
	if [ -x "/etc/splash/${SPLASH_THEME}/scripts/${event}-post" ]; then
		/etc/splash/"${SPLASH_THEME}"/scripts/${event}-post ${args}
	fi

	return 0
}

splash_setup() {
	# If it's already set up, let's not waste time on parsing the config
	# files again
	if [ "${SPLASH_THEME}" != "" -a "${SPLASH_TTY}" != "" -a "$1" != "force" ]; then
		return 0
	fi

	export SPLASH_EFFECTS=""
	export SPLASH_SANITY=""
	export SPLASH_TEXTBOX="no"
	export SPLASH_MODE_REQ="off"
	export SPLASH_PROFILE="off"
	export SPLASH_THEME="default"
	export SPLASH_TTY="16"
	export SPLASH_KDMODE="TEXT"
	export SPLASH_AUTOVERBOSE="0"
	export SPLASH_BOOT_MESSAGE="Booting the system (\$progress%)... Press F2 for verbose mode."
	export SPLASH_SHUTDOWN_MESSAGE="Shutting down the system (\$progress%)... Press F2 for verbose mode."
	export SPLASH_REBOOT_MESSAGE="Rebooting the system (\$progress%)... Press F2 for verbose mode."
	export SPLASH_XSERVICE="xdm"

	[ -f /etc/splash/splash ] && . /etc/splash/splash
	[ -f /etc/conf.d/splash ] && . /etc/conf.d/splash
	[ -f /etc/conf.d/fbcondecor ] && . /etc/conf.d/fbcondecor

	if [ -f /proc/cmdline ]; then
		options=$(grep -o 'splash=[^ ]*' /proc/cmdline)

		# Execute this loop over $options so that we can process multiple
		# splash= arguments on the kernel command line. Useful for adjusting
		# splash parameters from ISOLINUX.
		for opt in ${options} ; do
			options=${opt#*=}

			for i in $(echo "${options}" | sed -e 's/,/ /g') ; do
				case ${i%:*} in
					theme)		SPLASH_THEME=${i#*:} ;;
					tty)		SPLASH_TTY=${i#*:} ;;
					verbose) 	SPLASH_MODE_REQ="verbose" ;;
					silent)		SPLASH_MODE_REQ="silent" ;;
					kdgraphics)	SPLASH_KDMODE="GRAPHICS" ;;
					profile)	SPLASH_PROFILE="on" ;;
					insane)		SPLASH_SANITY="insane" ;;
				esac
			done
		done
	fi
}

splash_get_boot_message() {
	if [ "${RUNLEVEL}" = "6" ]; then
		echo "${SPLASH_REBOOT_MESSAGE}"
	elif [ "${RUNLEVEL}" = "0" ]; then
		echo "${SPLASH_SHUTDOWN_MESSAGE}"
	else
		echo "${SPLASH_BOOT_MESSAGE}"
	fi
}

splash_start() {
	if [ "${SPLASH_MODE_REQ}" = "verbose" ]; then
		${spl_decor} -c on 2>/dev/null
		return 0
	elif [ "${SPLASH_MODE_REQ}" != "silent" ]; then
		return 0
	fi

	# Display a warning if the system is not configured to display init messages
	# on tty1. This can cause a lot of problems if it's not handled correctly, so
	# we don't allow silent splash to run on incorrectly configured systems.
	if [ "${SPLASH_MODE_REQ}" = "silent" -a "${SPLASH_SANITY}" != "insane" ]; then
		if [ -z "$(grep -E '(^| )CONSOLE=/dev/tty1( |$)' /proc/cmdline)" -a \
			 -z "$(grep -E '(^| )console=tty1( |$)' /proc/cmdline)" ]; then
			clear
			splash_warn "You don't appear to have a correct console= setting on your kernel"
			splash_warn "command line. Silent splash will not be enabled. Please add"
			splash_warn "console=tty1 or CONSOLE=/dev/tty1 to your kernel command line"
			splash_warn "to avoid this message."
			if [ -n "$(grep 'CONSOLE=/dev/tty1' /proc/cmdline)" -o \
				  -n "$(grep 'console=tty1' /proc/cmdline)" ]; then
				splash_warn "Note that CONSOLE=/dev/tty1 and console=tty1 are general parameters and"
				splash_warn "not splash= settings."
			fi
			return 1
		fi

		if [ -n "$(grep -E '(^| )CONSOLE=/dev/tty1( |$)' /proc/cmdline)" ]; then
			mount -n --bind / ${spl_tmpdir}
			if [ ! -c "${spl_tmpdir}/dev/tty1" ]; then
				umount -n ${spl_tmpdir}
				splash_warn "The filesystem mounted on / doesn't contain the /dev/tty1 device"
				splash_warn "which is required for the silent splash to function properly."
				splash_warn "Silent splash will not be enabled. Please create the appropriate"
				splash_warn "device node to avoid this message."
				return 1
			fi
			umount -n ${spl_tmpdir}
		fi
	fi

	rm -f "${spl_pidfile}"

	# Prepare the communications FIFO
	rm -f "${spl_fifo}" 2>/dev/null
	mkfifo "${spl_fifo}"

	local options=""
	[ "${SPLASH_KDMODE}" = "GRAPHICS" ] && options="--kdgraphics"
	[ -n "${SPLASH_EFFECTS}" ] && options="${options} --effects=${SPLASH_EFFECTS}"
	[ "${SPLASH_TEXTBOX}" = "yes" ] && options="${options} --textbox"

	local ttype="bootup"
	if [ "${RUNLEVEL}" = "6" ]; then
		ttype="reboot"
	elif [ "${RUNLEVEL}" = "0" ]; then
		ttype="shutdown"
	fi

	# Start the splash daemon
	BOOT_MSG="$(splash_get_boot_message)" ${spl_daemon} --theme="${SPLASH_THEME}" --pidfile="${spl_pidfile}" --type=${ttype} ${options}

	# Set the silent TTY and boot message
	splash_comm_send "set tty silent ${SPLASH_TTY}"

	if [ "${SPLASH_MODE_REQ}" = "silent" ]; then
		splash_comm_send "set mode silent"
		splash_comm_send "repaint"
		${spl_decor} -c on 2>/dev/null
	fi

	splash_comm_send "set autoverbose ${SPLASH_AUTOVERBOSE}"

	splash_set_event_dev

	return 0
}

###########################################################################
# Cache-related functions
###########################################################################

splash_cache_prep() {
	# Mount an in-RAM filesystem at spl_cachedir
	mount -ns -t "${spl_cachetype}" cachedir "${spl_cachedir}" \
		-o rw,mode=0644,size="${spl_cachesize}"k

	retval="$?"

	if [ ${retval} -ne 0 ]; then
		splash_err "Unable to create splash cache - switching to verbose."
		splash_verbose
		return "${retval}"
	fi
}

# args: list of files to save when the cache is umounted
#       Note that the 'profile' file is already handled and thus shouldn't
#       be included in the list.
pgrep() {
ps |grep $1 |grep -v grep |awk '{print $1}'
}
splash_cache_cleanup() {
#echo " "
	# Don't try to clean anything up if the cachedir is not mounted.
	[ -z "$(grep ${spl_cachedir} /proc/mounts)" ] && return;

	# Create the temp dir if necessary.
	if [ ! -d "${spl_tmpdir}" ]; then
		mkdir -p "${spl_tmpdir}" 2>/dev/null
		[ "$?" != "0" ] && return
	fi

	# Make sure the splash daemon is dead.
	if [ -n "$(pgrep fbsplashd)" ]; then
		sleep 1
		killall -9 "${spl_daemon##*/}" 2>/dev/null
	fi

	# If /etc is not writable, don't update /etc/mtab. If it is
	# writable, update it to avoid stale mtab entries (bug #121827).
	#local mntopt=""
	#[ -w /etc/mtab ] || mntopt="-n"
	#mount ${mntopt} --move "${spl_cachedir}" "${spl_tmpdir}" 2>/dev/null

	# Don't try to copy anything if the cachedir is not writable.
	[ -w "${spl_cachedir}" ] || return

	if [ "${SPLASH_PROFILE}" != "off" ]; then
		cp -a "${spl_tmpdir}/profile" "${spl_cachedir}" 2>/dev/null
	fi

	while [ -n "$1" ]; do
		cp -a "${spl_tmpdir}/$1" "${spl_cachedir}" 2>/dev/null
		shift
	done

	umount -l "${spl_tmpdir}" 2>/dev/null
}

###########################################################################
# Common functions
###########################################################################

# Sends data to the splash FIFO after making sure there's someone
# alive on the other end to receive it.
splash_comm_send() {
	if [ -z "`pidof $(basename ${spl_daemon})`" ]; then
		return 1
	else
		splash_profile "comm $*"
		echo "$*" > "${spl_fifo}" &
	fi
}

# Returns the current splash mode.
splash_get_mode() {
	local ctty="${spl_bindir}/fgconsole"
	local mode="$(${spl_util})"

	if [ "${mode}" = "silent" ]; then
		echo "silent"
	else
		if [ -z "$(${spl_decor} -c getstate --tty=${ctty} 2>/dev/null | grep off)" ]; then
			echo "verbose"
		else
			echo "off"
		fi
	fi
}

# chvt <n>
# --------
# Switches to the n-th tty.
chvt() {
	local ntty=$1

	if [ -x /bin/chvt ] ; then
		/bin/chvt ${ntty}
	else
		printf "\e[12;${ntty}]"
	fi
}

# Switches to verbose mode.
splash_verbose() {
	chvt 1
}

# Switches to silent mode.
splash_silent() {
	splash_comm_send "set mode silent"
}

# Saves profiling information
splash_profile() {
	if [ "${SPLASH_PROFILE}" = "on" ]; then
		echo "$(cat /proc/uptime | cut -f1 -d' '): $*" >> "${spl_cachedir}/profile"
	fi
}

# Set the input device if it exists. This will make it possible to use F2 to
# switch from verbose to silent.
splash_set_event_dev() {
	local t="$(grep -Hsi keyboard /sys/class/input/input*/name | sed -e 's#.*input\([0-9]*\)/name.*#event\1#')"
	if [ -z "${t}" ]; then
		t="$(grep -Hsi keyboard /sys/class/input/event*/device/driver/description | grep -o 'event[0-9]\+')"
		if [ -z "${t}" ]; then
			for i in /sys/class/input/input* ; do
				if [ "$((0x$(cat $i/capabilities/ev) & 0x100002))" = "1048578" ]; then
					t="$(echo $i | sed -e 's#.*input\([0-9]*\)#event\1#')"
				fi
			done

			if [ -z "${t}" ]; then
				# Try an alternative method of finding the event device. The idea comes
				# from Bombadil <bombadil(at)h3c.de>. We're couting on the keyboard controller
				# being the first device handled by kbd listed in input/devices.
				t="$(/bin/grep -s -m 1 '^H: Handlers=kbd' /proc/bus/input/devices | grep -o 'event[0-9]*')"
			fi
		fi
	fi
	[ -n "${t}" ] && splash_comm_send "set event dev /dev/input/${t}"
}

###########################################################################
# Service
###########################################################################

# args: <svc> <action>
splash_svc() {
	local srv="$1"
	local act="$2"

	if [ "${act}" = "start" ]; then
		splash_svc_update "${srv}" "svc_started"
		if [ "${srv}" = "gpm" ]; then
			splash_comm_send "set gpm"
			splash_comm_send "repaint"
		fi
		splash_comm_send "log Service '${srv}' started."
	else
		splash_svc_update "${srv}" "svc_stopped"
		splash_comm_send "log Service '${srv}' stopped."
	fi

	splash_update_progress "${srv}"
}

# args: <svc> <action>
splash_svc_fail() {
	local srv="$1"
	local act="$2"

	if [ "${SPLASH_VERBOSE_ON_ERRORS}" = "yes"  ]; then
		splash_verbose
		return 1
	fi

	if [ "${act}" = "start" ]; then
		splash_svc_update "${srv}" "svc_start_failed"
		splash_comm_send "log Service '${srv}' failed to start."
	else
		splash_svc_update "${srv}" "svc_stop_failed"
		splash_comm_send "log Service '${srv}' failed to stop."
	fi

	splash_update_progress "${srv}"
}

# args: <svc> <state>
#
# Inform the splash daemon about service status changes.
splash_svc_update() {
	splash_comm_send "update_svc $1 $2"
}

# args: <svc>
splash_svc_start() {
	local svc="$1"

	splash_svc_update "${svc}" "svc_start"
	splash_comm_send "paint"
}

# args: <svc>
splash_svc_stop() {
	local svc="$1"

	splash_svc_update "${svc}" "svc_stop"
	splash_comm_send "paint"
}

# args: <svc>
splash_input_begin() {
	local svc="$1"

	if [ "$(splash_get_mode)" = "silent" ]; then
		splash_verbose
		export SPL_SVC_INPUT_SILENT="${svc}"
	fi
}

# args: <svc>
splash_input_end() {
	local svc="$1"

	if [ "${SPL_SVC_INPUT_SILENT}" = "${svc}" ]; then
		splash_silent
		unset SPL_SVC_INPUT_SILENT
	fi
}

###########################################################################
# Framebuffer Console Decorations functions
###########################################################################

fbcondecor_supported()
{
	[ -e /dev/fbsplash -o -e /dev/fbcondecor ]
}

# args: <theme> <tty>
fbcondecor_set_theme()
{
	local theme=$1
	local tty=$2

	[ -x ${spl_decor} ] || return 1

	${spl_decor} --tty="${tty}" -t "${theme}" -c setcfg || return 1
	${spl_decor} --tty="${tty}" -t "${theme}" -c setpic -q
	${spl_decor} --tty="${tty}" -c on
}

###########################################################################
# Service list
###########################################################################

# splash_svclist_get <type>
# -------------------------
# type:
#  - start - to get a list of services to be started during bootup
#  - stop  - to get a list of services to be stopped during shutdown/reboot
splash_svclist_get() {
	if [ "$1" = "start" -a -r "${spl_cachedir}/svcs_start" ]; then
		cat "${spl_cachedir}/svcs_start"
	elif [ "$1" = "stop" -a -r "${spl_cachedir}/svcs_stop" ]; then
		cat "${spl_cachedir}/svcs_stop"
	fi
}

splash_warn() {
	echo "$*"
}

splash_err() {
	echo "$*"
}

############################################################################
# Functions to be overridden by distro-specific code
###########################################################################

# args: <internal_runlevel> <runlevel>
#
# This function is called when an 'rc_init' event takes place,
# i.e. when the runlevel is changed.

# It is normally used to initialize any internal splash-related 
# variables (e.g. ones used to track the boot progress), calculate
# and save the service list and call `splash_start` in appropriate 
# runlevels.
#
# Note that the splash cache is already intialized when this
# function is called.
splash_init() {
	if [ "$2" = "6" -o "$2" = "0" -o "$2" = "S" -a "$1" = "sysinit" ]; then
		splash_start
	fi
}

# args: <runlevel>
#
# This function is called when an 'rc_exit' event takes place,
# i.e. at the end of processes all initscript from a runlevel.
splash_exit() {
	# If we're in sysinit or rebooting, do nothing.
	[ "$1" = "S" -o "$1" = "6" -o "$1" = "0" ] && return 0

	splash_comm_send "exit"
	splash_cache_cleanup
}

force_splash_exit()
{
	splash_comm_send "exit"
        splash_cache_cleanup
}

# args: <svc>
#
# This function is called whenever the progress variable should be
# updated.  It should recalculate the progress and send it to the
# splash daemon.
#splash_update_progress() {
#	 splash_comm_send "progress ${progress}"
#	 splash_comm_send "paint"
#	return
#}

# Export functions if we're running bash.
if [ -n "${BASH}" ]; then
	export -f splash
	export -f splash_setup
	export -f splash_get_boot_message
	export -f splash_start
	export -f splash_cache_prep
	export -f splash_cache_cleanup
	export -f splash_comm_send
	export -f splash_get_mode
	export -f chvt
	export -f splash_verbose
	export -f splash_silent
	export -f splash_profile
	export -f splash_set_event_dev
	export -f splash_svclist_get
fi

is_enabled()
{
	local switch
	if [ ! -z "$1" ]; then
		value=`make_caps $1`
		for i in TRUE ON ENABLED YES POSITIVE Y; do
			if [ $value == $i ]; then
				switch=0
			fi
		done
	fi
        if [ "$switch" == "0" ] ; then
		return 0
	else
		return 1
	fi
}

is_disabled()
{
        local switch
        if [ ! -z "$1" ]; then
                value=`make_caps $1`
        	for i in FALSE OFF DISABLED NO NEGATIVE N ; do
                	if [ $value == $i ]; then
                        	switch=0
                	fi
        	done
	else
		switch=0
	fi
        if [ "$switch" == "0" ] ; then
                return 0
        else
                return 1
        fi
}

screen_res()
{
	if [ -z "$SCREEN_RESOLUTION" ]; then
		video="`cat /proc/cmdline |grep -o -E video=[a-zA-Z0-9:,-]+ |head -n 1 |cut -d = -f2`"
		if [ "$video" != "uvesafb:off" ]; then
			SCREEN_RESOLUTION=`echo $video |cut -d ":" -f2 |cut -d "-" -f1`
		else
			SCREEN_RESOLUTION=$PREFERED_RES
        	fi
	fi
}

use_xrandr()
{
	if is_enabled $USE_XRANDR ; then
        	echo "Using xrandr to set display resolution." >> $LOGFILE
		sleep .25
        	if [ "$XRANDR_OPTIONS" == "" ]; then
		    	xrandr -s $SCREEN_RESOLUTION
        	else
                	xrandr $XRANDR_OPTIONS
        	fi
	fi
}

use_wallpaper()
{
	if is_enabled $USE_WALLPAPER && [ -e /bin/wmsetbg ] ; then
        	if [ -f /etc/background.jpg ] ; then
                	wmsetbg /etc/background.jpg 2>/dev/null
        	elif [ -f /etc/background-$SCREEN_RESOLUTION.jpg ] ; then
                	wmsetbg /etc/background-$SCREEN_RESOLUTION.jpg 2>/dev/null
        	elif [ -f /etc/background-1024x768.jpg ]; then
                	wmsetbg /etc/background-1024x768.jpg 2>/dev/null
		else
			echo_log "No wallpaper found"
        	fi
	fi
}

x_mod_map()
{
 # Add xmodmap modifications
        if [ -e /lib/kmaps/$KEYBOARD_MAP.xmod ] ; then
                xmodmap /lib/kmaps/$KEYBOARD_MAP.xmod
        fi
}

x_numlock()
{
        if is_enabled $X_NUMLOCK ; then
                numlockx `make_lower $X_NUMLOCK`
        fi
}

x_auth_file()
{
	if [ ! -e $HOME/.Xauthority ] ; then
		touch $HOME/.Xauthority
		xauth -f $HOME/.Xauthority generate :$DISPLAY_NUMBER.0 . trusted >> $LOGFILE 2>&1
	fi
}

x_mouse()
{
#         xsetroot -cursor_name top_left_arrow >> $LOGFILE 2>&1 &
          if [ -n "$MOUSE_ACCELERATION" ]; then 
                echo xset -display :$DISPLAY_NUMBER m ${MOUSE_ACCELERATION} 1 >> $LOGFILE
                xset -display :$DISPLAY_NUMBER m ${MOUSE_ACCELERATION} 1 >> $LOGFILE 2>&1 &
          fi
}
