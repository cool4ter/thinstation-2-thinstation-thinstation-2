table Config
 FontName: -misc-fixed-medium-r-semicondensed--13-120-75-75-c-60-iso8859-*
 FontColor: #FFFFFF
 ShadowColor: #505050
 CaptionBackground: no
 CaptionBorder: yes
 ClickInterval: 500
 IconWidth: 32
 IconHeight: 32
 Fixed: true
 SnapWidth: 16
 SnapHeight: 16
 FontSize: 8
 ToolTip.FontSize: 9
 ToolTip.FontName: gothic
 ToolTip.ForeColor: #0000FF
 ToolTip.BackColor: #FFFFFF
 ToolTip.CaptionOnHover: true
 ToolTip.CaptionPlacement: right
 Locked: true
 Transparency: 100
 Shadow: false
 ShadowX: 1
 ShadowY: 2
 Bold: false
 ClickDelay: 300
 IconSnap: true
 SnapOrigin: BottomRight
 SnapShadow: true
 SnapShadowTrans: 200
 CaptionOnHover: false
 CaptionPlacement: bottom
 FillStyle: FillHLine
 Background.Delay: 0
 Background.Source: None
 Background.File: None 
 Background.Mode: Center 
 Background.Color: #C2CCFF
 CursorOver: hand2
end
table Actions
 Lock: control right doubleClk
 Reload: middle doubleClk
 Drag: left hold
 EndDrag: left singleClk
 Execute[0]: left doubleClk
 Execute[1]: right doubleClk
end
